import os
from dotenv import load_dotenv

from pymongo import MongoClient
from datetime import datetime

load_dotenv("env.env")
password = os.getenv("MONGO_PW")
connection = "mongodb+srv://Breaker:" + password + "@cluster0.barzx.mongodb.net/<dbname>?retryWrites=true&w=majority"
cluster = MongoClient(connection)
database = cluster["discord"]
players = database["players"]
ranks = database["roles"]
matches = database["matches"]


def CreateNewProfile(_id, name, rank, points, game, server):
    return_code = None
    if CheckPlayerExists(_id, game, server) is True:
        return_code = 0
    else:
        post = {"discord_id": _id, "name": name, "rank": rank, "points": points, "game": game, "games": 0, "wins": 0, "server": server}
        players.insert_one(post)
        return_code = 1
    return return_code


def CheckPlayerExists(_id, game, server):
    search = {"discord_id": _id, "game": game, "server": server}
    return players.find_one(search) is not None


def GetPlayerProfile(_id, game, server):
    search = {"discord_id": _id, "game": game, "server": server}
    return players.find_one(search)


def AddAdmin(role, server):
    post = {"game": "Admin", "role": role, "server": server}
    ranks.insert_one(post)


def UpdateAdmin(role, server):
    search = {"game": "Admin", "server": server}
    post = {"$set": {"role": role}}
    ranks.update_one(search, post)


def CheckAdmin(server):
    post = {"server": server, "game": "Admin"}
    return ranks.find_one(post)


def AddRank(role, game, server, _min, _max, role_id):
    post = {"game": game, "role": role, "min": _min, "max": _max, "server": server, "role_id": role_id}
    ranks.insert_one(post)


def RemoveRank(role, game, server):
    post = {"game": game, "role": role, "server": server}
    ranks.remove_one(post)


def GetRanks(game, server):
    search = {"game": game, "server": server}
    return ranks.find(search)


def SetDefaultRank(role, points, game, server):
    post = {"role_id": role.id, "game": game + "-Default", "role": role.name, "points": points, "server": server}
    ranks.insert_one(post)


def GetDefaultRank(game, server):
    search = {"game": game + "-Default", "server": server}
    return ranks.find_one(search)


def UpdateRank(_id, game, server, rank):
    search = {"game": game, "server": server, "discord_id": _id}
    post = {"$set": {"rank": rank}}
    players.update_one(search, post)


def UpdatePoints(_id, game, server, points):
    search = {"game": game, "server": server, "discord_id": _id}
    post = {"$set": {"points": points}}
    players.update_one(search, post)


def GetLeaderboard(game, server):
    search = {"game": game, "server": server}
    return players.find(search)


def ReportMatch(game, server, winner, loser, difference):
    post = {"game": game, "server": server,
            "winner_id": winner["discord_id"], "winner": winner["name"],
            "loser_id": loser["discord_id"], "loser": loser["name"],
            "date": datetime.now().strftime("%d/%m/%Y %H:%M:%S")}
    matches.insert_one(post)
    search = {"game": game, "server": server, "discord_id": winner["discord_id"]}
    post = {"$set": {"points": winner["points"], "games": int(winner["games"]) + 1, "wins": int(winner["wins"]) + 1}}
    players.update_one(search, post)
    search = {"game": game, "server": server, "discord_id": loser["discord_id"]}
    post = {"$set": {"points": loser["points"], "games": int(loser["games"]) + 1}}
    players.update_one(search, post)


def GetMatchHistory(_id, game, server):
    search = {"$or": [{"winner_id": _id}, {"loser_id": _id}], "game": game, "server": server}
    return matches.find(search)


def GetServerMatchHistory(game, server):
    search = {"game": game, "server": server}
    return matches.find(search)
