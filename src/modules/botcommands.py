import os
import discord
import json
import random
import sys

from discord.ext import commands
from dotenv import load_dotenv

import modules.botfunctions as bot_functions

load_dotenv("env.env")
TOKEN = os.getenv('DISCORD_TOKEN')

def start():
    phrases = None
    bot = commands.Bot(command_prefix='j!')
    bot.remove_command('help')

    with open('resources/phrases.json', 'rb') as f:
        phrases = json.load(f)

    @bot.event
    async def on_ready():
        print(f'{bot.user} has connected to Discord!')
        await bot.change_presence(activity=discord.Game("Use j!help"))

    @bot.command(name="register")
    async def Register(ctx, name: discord.Member, rank: discord.Role, points: int, game):
        if await bot_functions.AdminCheck(ctx, ctx.message.author) != 2:
            return

        msg = await ctx.send(bot_functions.GetPhrase('profile_new'))
        return_code = bot_functions.CreateNewProfile(name.id, name.display_name, rank.name, points, game, ctx.guild.name)
        if return_code == 0:
            await msg.edit(content=bot_functions.GetPhrase('fail_user_exists'))
            return

        if return_code == 1:
            await name.add_roles(rank)
            await bot_functions.GetProfile(ctx, name.id, game, ctx.guild.name, False)

    @bot.command(name="registerself")
    async def RegisterSelf(ctx, game):
        msg = await ctx.send(bot_functions.GetPhrase('profile_new'))
        name = ctx.message.author
        return_code = bot_functions.CheckUserExists(name.id, game, ctx.guild.name)
        if return_code:
            await msg.edit(content=bot_functions.GetPhrase('fail_user_exists'))
            return

        await bot_functions.CreateNewProfileSelf(ctx, name, game, ctx.guild.name)



    @bot.command(name="uprofile")
    async def UpdateProfile(ctx, name: discord.Member, rank: discord.Role, points: int, game):
        if await bot_functions.AdminCheck(ctx, ctx.message.author) != 2:
            return

        if not bot_functions.CheckUserExists(name.id, game, ctx.guild.name):
            await ctx.send(bot_functions.GetPhrase('fail_user_missing'))
            return

        await ctx.send(bot_functions.GetPhrase('profile_updating'))
        await bot_functions.UpdateRank(name.id, game, rank.name, ctx.guild.name)
        await bot_functions.UpdateRole(ctx, name, rank, game, ctx.guild.name)
        await bot_functions.UpdatePoints(name.id, game, ctx.guild.name, points)
        await bot_functions.GetProfile(ctx, name.id, game, ctx.guild.name, False)

    @bot.command(name="urank")
    async def UpdateRank(ctx, name: discord.Member, rank: discord.Role, game):
        if await bot_functions.AdminCheck(ctx, ctx.message.author) != 2:
            return

        if not bot_functions.CheckUserExists(name.id, game, ctx.guild.name):
            await ctx.send(bot_functions.GetPhrase('fail_user_missing'))
            return

        await ctx.send(bot_functions.GetPhrase('profile_updating_rank'))
        await bot_functions.UpdateRank(name.id, game, ctx.guild.name, rank.name)
        await bot_functions.UpdateRole(ctx, name, rank, game, ctx.guild.name)
        await bot_functions.GetProfile(ctx, name.id, game, ctx.guild.name, False)

    @bot.command(name="upoints")
    async def UpdatePoints(ctx, name: discord.Member, points: int, game):
        if await bot_functions.AdminCheck(ctx, ctx.message.author) != 2:
            return

        if not bot_functions.CheckUserExists(name.id, game, ctx.guild.name):
            await ctx.send(bot_functions.GetPhrase('fail_user_missing'))
            return

        await ctx.send(bot_functions.GetPhrase('profile_updating_points'))
        await bot_functions.UpdatePoints(name.id, game, ctx.guild.name, points)
        await bot_functions.GetProfile(ctx, name.id, game, ctx.guild.name, False)

    @bot.command(name="rgame", aliases=['report', 'reportgame', 'reportmatch'])
    async def Report(ctx, player1: discord.Member, player2: discord.Member, winner, game):

        reporting_check = await bot_functions.ReportChecks(ctx, player1, player2, game, winner)
        if reporting_check == 0:
            return

        await ctx.send(bot_functions.GetPhrase('reporting_success'))
        await bot_functions.Report(ctx, player1.id, player2.id, game, ctx.guild.name, winner)

        await ctx.send(bot_functions.GetPhrase('profile_get_updated'))
        await bot_functions.GetProfile(ctx, player1.id, game, ctx.guild.name, False)
        await bot_functions.GetProfile(ctx, player2.id, game, ctx.guild.name, False)

    @bot.command(name="leaderboard", aliases=['leaderboards'])
    async def Leaderboard(ctx, game, length=10, page=0):
        await bot_functions.GetLeaderboard(ctx, game, ctx.guild.name, length, page)

    @bot.command(name="profile")
    async def Profile(ctx, name: discord.Member, game):
        await bot_functions.GetProfile(ctx, name.id, game, ctx.guild.name, True)

    @bot.command(name="talkshit")
    async def TalkShit(ctx, name: discord.Member = None):

        with open('resources/insults.json', encoding="utf8") as f:
            insults = json.load(f)
        if name is None:
            name = ctx.message.author
        elif bot.user.mention == name.mention:
            await ctx.send("*" + bot_functions.GetPhrase('talkshit_self')
                           + ctx.message.author.mention + ", " + random.choice(insults['insults'])['insult'] + "* ")
            return
        await ctx.send("*Hey " + name.mention + ", " + random.choice(insults['insults'])['insult'] + "* ")

    @bot.command(name="iadmin", hidden=True)
    async def InitializeAdmin(ctx, role: discord.Role):
        if await bot_functions.AdminCheck(ctx, ctx.message.author) == 2:
            await ctx.send(bot_functions.GetPhrase('fail_admin_set'))
            return

        msg = await ctx.send(bot_functions.GetPhrase('init_admin_0'))
        bot_functions.InitializeAdmin(role.name, ctx.guild.name)
        await msg.edit(content=bot_functions.GetPhrase('init_admin_1') + role.mention)
        await ctx.send(bot_functions.GetPhrase('init_admin_2'))

    @bot.command(name="uadmin", hidden=True)
    async def UpdateAdmin(ctx, role: discord.Role):
        if await bot_functions.AdminCheck(ctx, ctx.message.author) != 2:
            return

        msg = await ctx.send(bot_functions.GetPhrase('init_admin_0'))
        bot_functions.UpdateAdmin(role.name, ctx.guild.name)
        await msg.edit(content=bot_functions.GetPhrase('init_admin_1') + role.mention)
        await ctx.send(bot_functions.GetPhrase('init_admin_2'))

    @bot.command(name="crank", hidden=True)
    async def CreateRank(ctx, role: discord.Role, game, _min: int, _max: int):
        if await bot_functions.AdminCheck(ctx, ctx.message.author) != 2:
            return

        msg = await ctx.send(bot_functions.GetPhrase('init_rank'))
        bot_functions.CreateRank(role.name, game, ctx.guild.name, _min, _max, role.id)
        await msg.edit(content=bot_functions.GetPhrase('init_rank_1') + "Role: " + role.mention + " Min " + _min + " Max " + _max)

    @bot.command(name="rrank", hidden=True)
    async def RemoveRank(ctx, role: discord.Role, game):
        if await bot_functions.AdminCheck(ctx, ctx.message.author) != 2:
            return

        bot_functions.RemoveRank(role.name, game, ctx.guild.name)
        await ctx.send(bot_functions.GetPhrase('remove_rank'))

    @bot.command(name="drank", hidden=True)
    async def SetDefaultRank(ctx, role: discord.Role, points: int, game):
        if await bot_functions.AdminCheck(ctx, ctx.message.author) != 2:
            return

        bot_functions.SetDefaultRank(role, points, game, ctx.guild.name)
        await ctx.send(bot_functions.GetPhrase('default_rank'))

    @bot.command(name="ranks", hidden=True)
    async def GetRanks(ctx, game):
        await ctx.send(bot_functions.GetRanks(game, ctx.guild.name))

    @bot.command(name="history", aliases=['matchhistory'])
    async def GetMatchHistory(ctx, name: discord.Member, game, length=10, page=0):

        if not bot_functions.CheckUserExists(name.id, game, ctx.guild.name):
            await ctx.send(bot_functions.GetPhrase('fail_user_missing'))
            return

        await bot_functions.GetMatchHistory(ctx, name.id, game, ctx.guild.name, length, page)

    @bot.command(name="shistory", aliases=['smatchhistory'])
    async def GetServerMatchHistory(ctx, game, length=10, page=0):
        await bot_functions.GetServerMatchHistory(ctx, game, ctx.guild.name, length, page)

    @bot.command(name="help")
    async def Help(ctx, query: str = None):
        if query == "moderation":
            if bot_functions.AdminCheck(ctx, ctx.message.author) != 2:
                await ctx.send(bot_functions.GetPhrase('fail_permissions'))
                return
        await bot_functions.Help(ctx, query)

    @bot.command(name="dev")
    async def Dev(ctx):
        await ctx.send(bot_functions.GetPhrase("dev"))


    '''
    @bot.command(name="wregister")
    async def RegisterWeekly(ctx):
        msg = await ctx.send(bot_functions.GetPhrase('weekly_registering'))
        doc_id = bot_functions.GetRoles(ctx.guild.name)
        doc_id = doc_id['doc']
        name = ctx.message.author
        await bot_functions.RegisterWeekly(ctx, sheet, name, doc_id)
        await msg.edit(content=bot_functions.GetPhrase('weekly_registered'))

    @bot.command(name="wunregister")
    async def UnregisterWeekly(ctx):
        msg = await ctx.send(bot_functions.GetPhrase('weekly_unregistering'))
        name = ctx.message.author
        await bot_functions.UnregisterWeekly(ctx, name)
        await msg.edit(content=bot_functions.GetPhrase('weekly_unregistered'))

    @bot.command(name="wlist")
    async def ListWeekly(ctx):
        await bot_functions.ListWeekly(ctx)

    @bot.command(name="wstart")
    async def StartWeekly(ctx):
        if not await bot_functions.AdminCheck(ctx, ctx.message.author):
            await ctx.send(bot_functions.GetPhrase('fail_permissions'))
            return
        await bot_functions.StartWeekly(ctx)

    @bot.command(name="wend")
    async def EndWeekly(ctx):
        if not await bot_functions.AdminCheck(ctx, ctx.message.author):
            await ctx.send(bot_functions.GetPhrase('fail_permissions'))
            return
        await bot_functions.EndWeekly(ctx)

    @bot.command(name="wcheckin")
    async def CheckinWeekly(ctx):
        name = ctx.message.author
        await bot_functions.CheckinWeekly(ctx, name)

    @bot.command(name="wtoggle")
    async def CheckinWeeklyToggle(ctx):
        if not await bot_functions.AdminCheck(ctx, ctx.message.author):
            await ctx.send(bot_functions.GetPhrase('fail_permissions'))
            return
        await bot_functions.ToggleWeeklyCheckin(ctx)

    @bot.command(name="test")
    async def Help(ctx, name: discord.Member):
        await ctx.send(name.id)
        await ctx.send(name.nick)
        await ctx.send(name.display_name)
    '''
    @bot.event
    async def on_command_error(ctx, error):
        if isinstance(error, discord.ext.commands.errors.CommandNotFound):
            await ctx.send(bot_functions.GetPhrase('fail_command_not_found'))

        elif isinstance(error, discord.ext.commands.errors.BadArgument):
            await ctx.send(bot_functions.GetPhrase('fail_bad_arguments'))

        elif isinstance(error, discord.ext.commands.errors.MissingRequiredArgument):
            await ctx.send(bot_functions.GetPhrase('fail_missing_arguments'))

        else:
            await ctx.send(bot_functions.GetPhrase('fail_unknown_error'))

        print("Error Origin:", ctx.guild.name,)
        print("Error Author:", ctx.message.author.display_name, ctx.message.author.id)
        print("Error Message:", ctx.message.content)
        print("Error:")
        raise error

    bot.run(TOKEN)
