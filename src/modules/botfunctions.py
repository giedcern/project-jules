from prettytable import PrettyTable
from operator import itemgetter
import json
import os.path
from os import path
import modules.database as database
import operator

phrases = None


# Function to calculate the Probability
def Probability(rating1, rating2):
    return 1.0 * 1.0 / (1 + 1.0 * pow(10, 1.0 * (rating1 - rating2) / 400))


# Function to calculate Elo rating
# K is a constant.
# d determines whether
# Player A wins or Player B.
def EloRating(Ra, Rb, K, d):
    # To calculate the Winning
    # Probability of Player B
    Pb = Probability(Ra, Rb)

    # To calculate the Winning
    # Probability of Player A
    Pa = Probability(Rb, Ra)

    # Case -1 When Player A wins
    # Updating the Elo Ratings
    if d == "1":
        Ra = Ra + K * (1 - Pa)
        Rb = Rb + K * (0 - Pb)

        # Case -2 When Player B wins
    # Updating the Elo Ratings
    else:
        Ra = Ra + K * (0 - Pa)
        Rb = Rb + K * (1 - Pb)

    return [round(Ra), round(Rb)]
    # This code is contributed by
    # Smitha Dinesh Semwal


async def Report(ctx, _id1, _id2, game, server, winner):
    profiles = [database.GetPlayerProfile(_id1, game, server), database.GetPlayerProfile(_id2, game, server)]
    roles = database.GetRanks(game, server)
    roles_sorted = sorted(roles, key=operator.itemgetter("max"), reverse=True)
    k_factor = DeterminePoints(profiles, winner, roles_sorted)
    new_elo = EloRating(int(profiles[0]["points"]), int(profiles[1]["points"]), k_factor, winner)
    points_difference = abs(profiles[0]["points"] - new_elo[0])
    profiles[0]["points"] = new_elo[0]
    profiles[1]["points"] = new_elo[1]

    for profile in profiles:
        new_rank = None
        for i in range(0, len(roles_sorted)):
            if profile["rank"] == roles_sorted[i]["role"]:
                if profile["points"] < int(roles_sorted[i]["min"]):
                    new_rank = roles_sorted[i + 1]
                    await ctx.send(profile["name"] + GetPhrase("reporting_demotion"))
                elif profile["points"] >= int(roles_sorted[i]["max"]):
                    new_rank = roles_sorted[i - 1]
                    await ctx.send(profile["name"] + GetPhrase("reporting_promotion"))

        if new_rank is not None:
            member = await ctx.guild.fetch_member(profile["discord_id"])
            await UpdateRole(ctx, member, ctx.guild.get_role(new_rank["role_id"]), game, server)
            await UpdateRank(profile["discord_id"], game, server, new_rank["role"])

    await ctx.send(GetPhrase("reporting_points") + str(points_difference))
    if winner == "1":
        database.ReportMatch(game, server, profiles[0], profiles[1], abs(new_elo[0] - new_elo[1]))
    else:
        database.ReportMatch(game, server, profiles[1], profiles[0], abs(new_elo[0] - new_elo[1]))


def DeterminePoints(values, winner, roles):
    winner = int(winner) - 1
    if int(values[winner]["games"]) < 30 and int(values[winner]["points"]) < int(
            roles[0]["min"]):  # games less than 30, rating less than highest rank
        return 40
    elif int(values[winner]["games"]) >= 30 and int(values[winner]["points"]) < int(
            roles[0]["min"]):  # games more than 30, rating less than highest rank
        return 20
    elif int(values[winner]["points"]) >= int(roles[0]["min"]):  # rating higher than max rank
        return 10
    else:
        return 10


async def ReportChecks(ctx, player1, player2, game, winner):
    if winner != "1" and winner != "2":
        await ctx.send(GetPhrase('reporting_arguments'))
        return 0

    if player1.id == player2.id:
        await ctx.send(GetPhrase('reporting_same'))
        return 0

    if ctx.message.author.id is not player1.id and ctx.message.id is not player2.id:
        await ctx.send(GetPhrase('reporting_not_self'))
        return 0

    if not CheckUserExists(player1.id, game, ctx.guild.name) or not CheckUserExists(
            player2.id, game, ctx.guild.name):
        await ctx.send(GetPhrase('fail_user_missing'))
        return 0

    return 1


def CreateNewProfile(_id, name, rank, points, game, server):
    return database.CreateNewProfile(_id, name, rank, points, game, server)


async def CreateNewProfileSelf(ctx, name, game, server):
    default_rank = GetDefaultRank(game, server)
    created = CreateNewProfile(name.id, name.display_name, default_rank["role"], default_rank["points"], game, server)
    if created == 1:
        await name.add_roles(ctx.guild.get_role(default_rank["role_id"]))
        await GetProfile(ctx, name.id, game, ctx.guild.name, False)


def CheckUserExists(_id, game, server):
    return database.CheckPlayerExists(_id, game, server)


async def UpdateRank(_id, game, server, rank):
    database.UpdateRank(_id, game, server, rank)


async def UpdateRole(ctx, name, new_rank, game, server):
    ranks = database.GetRanks(game, server)
    for rank in ranks:
        await name.remove_roles(ctx.guild.get_role(rank["role_id"]))
    await name.add_roles(new_rank)


async def UpdatePoints(_id, game, server, points):
    database.UpdatePoints(_id, game, server, points)


def VerifyData(values, values_count):
    for i in range(0, values_count):
        try:
            gotdata = values[i]
            print(gotdata)
        except IndexError:
            values.append("N/A")
    return values


async def GetPlayerData(_id, game, server):
    return database.GetPlayerProfile(_id, game, server)


async def GetProfile(ctx, _id, game, server, check_for_player):
    msg = await ctx.send(GetPhrase('profile_getting'))
    if check_for_player:
        if database.CheckPlayerExists(_id, game, server) == 0:
            await msg.edit(content=GetPhrase('fail_user_missing'))
            return

    player = await GetPlayerData(_id, game, server)
    table = []
    field_names = ["Name", "Rank", "Points", "Game", "Games", "Wins"]
    del player["discord_id"]
    table.append(FormatResponse(player))
    await ctx.send(FormatTable(field_names, table))
    await msg.edit(content=GetPhrase('profile_found') + str(player["name"]))


def PageLength(len_table, length, page):
    if length < 0:
        length = 10
    if page < 0:
        page = 0

    length = min(len_table, length)
    page = min(int(len_table/length) - 1, page)
    table_from = 0+page*length
    table_to = page*length+length
    return table_from, table_to


async def GetLeaderboard(ctx, game, server, length, page):
    msg = await ctx.send(GetPhrase('leaderboard_getting'))
    leaderboard = database.GetLeaderboard(game, server)

    lb_sorted = sorted(leaderboard, key=operator.itemgetter("points"), reverse=True)
    field_names = ['Name', 'Rank', 'Points']
    table = []
    for rank in lb_sorted:
        del rank['wins']
        del rank['games']
        del rank['game']
        del rank['discord_id']
        table.append(FormatResponse(rank))

    if len(table) == 0:
        await msg.edit(content=GetPhrase("leaderboard_notfound"))
        return

    all_games = len(table)
    lb_from, lb_to = PageLength(len(table), length, page)
    table = table[lb_from:lb_to]
    await msg.edit(content=GetPhrase('leaderboard_found') + str(lb_from+1) + " - " + str(lb_to) + " / " + str(all_games))
    await ctx.send(FormatTable(field_names, table))


def FormatTable(field_names, data):
    lt = PrettyTable()
    lt.field_names = field_names
    for row in data:
        try:
            new_row = VerifyData(row, len(field_names))
            lt.add_row(new_row)
        except IndexError:
            break

    return "```Prolog\n" + str(lt) + "```"


def FormatResponse(data):
    values = []
    del data['_id']
    del data['server']
    for key in data:
        values.append(DecodeUTF(data[key]))
    return values


async def GetMatchHistory(ctx, _id, game, server, length, page):
    msg = await ctx.send(GetPhrase('profile_history'))
    matches = database.GetMatchHistory(_id, game, server)
    if matches is None:
        await msg.edit(content=GetPhrase("profile_nogames"))
        return

    table = []
    for match in matches:
        del match['loser_id']
        del match['winner_id']
        table.append(FormatResponse(match))
    table.reverse()

    if len(table) == 0:
        await msg.edit(content=GetPhrase("profile_nogames"))
        return

    all_games = len(table)
    lb_from, lb_to = PageLength(len(table), length, page)
    table = table[lb_from:lb_to]
    field_names = [ 'Game', 'Winner', 'Loser', 'Date']
    await msg.edit(content=GetPhrase('profile_games') + str(lb_from+1) + " - " + str(lb_to) + " / " + str(all_games))
    await ctx.send(FormatTable(field_names, table))


async def GetServerMatchHistory(ctx, game, server, length, page):
    msg = await ctx.send(GetPhrase('profile_history'))
    matches = database.GetServerMatchHistory(game, server)

    table = []
    for match in matches:
        del match['loser_id']
        del match['winner_id']
        table.append(FormatResponse(match))
    table.reverse()

    if len(table) == 0:
        await msg.edit(content=GetPhrase("history_none"))
        return

    all_games = len(table)
    lb_from, lb_to = PageLength(len(table), length, page)
    table = table[lb_from:lb_to]
    field_names = ['Game', 'Winner', 'Loser', 'Date']
    await msg.edit(content=GetPhrase('profile_games') + str(lb_from+1) + " - " + str(lb_to) + " / " + str(all_games))
    await ctx.send(FormatTable(field_names, table))


async def RegisterWeekly(ctx, sheet, name, doc_id):
    sheet_range = 'Profile_' + str(name) + '!A2:D2'
    result = sheet.values().get(spreadsheetId=doc_id,
                                range=sheet_range).execute()
    values = result.get('values', [])
    await UpdateWeeklyRegistration(ctx, values[0], name)


async def UnregisterWeekly(ctx, name):
    server_name = ctx.guild.name
    unfiltered_list = {'checkin_started': False, 'players': []}
    if path.exists("serverfiles/tournaments/" + server_name + "-unfiltered.json"):
        with open("serverfiles/tournaments/" + server_name + "-unfiltered.json", encoding="utf8") as f:
            registered_participants = json.load(f)
        unfiltered_list['checkin_started'] = registered_participants['checkin_started']
        for player in registered_participants['players']:
            if player['name'] != str(name):
                unfiltered_list['players'].append(player)
        with open('serverfiles/tournaments/' + ctx.guild.name + '-unfiltered.json', 'w+', encoding='utf-8') as f:
            json.dump(unfiltered_list, f, ensure_ascii=False, indent=4)


async def UpdateWeeklyRegistration(ctx, values, player):
    name = str(player)
    server_name = ctx.guild.name
    unfiltered_list = {'checkin_started': False, 'players': []}
    already_registered = False

    if path.exists("serverfiles/tournaments/" + server_name + "-unfiltered.json"):
        with open("serverfiles/tournaments/" + server_name + "-unfiltered.json", encoding="utf8") as f:
            registered_participants = json.load(f)
        unfiltered_list['checkin_started'] = registered_participants['checkin_started']
        for player in registered_participants['players']:
            if player['name'] == name:
                already_registered = True
            unfiltered_list['players'].append(player)
        if not already_registered:
            unfiltered_list['players'].append({'name': name, 'points': int(values[2]), 'checked_in': False})
            with open('serverfiles/tournaments/' + ctx.guild.name + '-unfiltered.json', 'w+', encoding='utf-8') as f:
                json.dump(unfiltered_list, f, ensure_ascii=False, indent=4)
        else:
            await ctx.send(GetPhrase('weekly_registration_already_registered'))

    else:
        unfiltered_list['players'].append({'name': name, 'points': int(values[2]), 'checked_in': False})
        with open('serverfiles/tournaments/' + ctx.guild.name + '-unfiltered.json', 'w+', encoding='utf-8') as f:
            json.dump(unfiltered_list, f, ensure_ascii=False, indent=4)


async def ListWeekly(ctx):
    msg = await ctx.send(GetPhrase('weekly_participants'))
    server_name = ctx.guild.name
    sorted_list = {'players': []}
    if path.exists("serverfiles/tournaments/" + server_name + "-unfiltered.json"):
        with open("serverfiles/tournaments/" + server_name + "-unfiltered.json", encoding="utf8") as f:
            registered_participants = json.load(f)
    else:
        await msg.edit(content=GetPhrase('weekly_registration_missing'))
        return
    for player in registered_participants['players']:
        sorted_list['players'].append(player)
    sorted_list['players'] = sorted(sorted_list['players'], key=itemgetter('points'), reverse=True)
    lt = PrettyTable()
    lt.field_names = ['#', 'Name', 'Points', 'Checked In']
    player_index = 0
    bracket_index = 0
    for player in sorted_list['players']:
        checkedin_status = player['checked_in']
        if checkedin_status == True:
            checkedin_status = "Yes"
        else:
            checkedin_status = "No"

        lt.add_row([bracket_index+1, player['name'], player['points'], checkedin_status])
        player_index += 1
        if player_index >= 8:
            lt.add_row(["-", "---", "---", "---"])
            bracket_index += 1
            player_index = 0
    weekly_list = str(lt)
    if len(weekly_list) >= 1990:
        first_list = True
        print(weekly_list)
        split_list = weekly_list.split("| - |")
        print("---")
        for new_list in split_list:
            print(new_list)
            if not first_list:
                new_list = "| - |" + new_list
            else:
                first_list = False
            print("after add")
            print(new_list)
            await ctx.send('```\n' + new_list + '```')
    else:
        await ctx.send('```\n' + str(lt) + '```')
    await msg.delete()


async def StartWeekly(ctx):
    server_name = ctx.guild.name
    msg = await ctx.send(GetPhrase('weekly_registration_start'))
    if path.exists("serverfiles/tournaments/" + server_name + "-unfiltered.json"):
        await msg.edit(content=GetPhrase('weekly_registration_already'))
    else:
        unfiltered_list = {'checkin_started': False, 'players': []}
        with open('serverfiles/tournaments/' + ctx.guild.name + '-unfiltered.json', 'w+', encoding='utf-8') as f:
            json.dump(unfiltered_list, f, ensure_ascii=False, indent=4)
        await msg.edit(content=GetPhrase('weekly_registration_active'))


async def EndWeekly(ctx):
    server_name = ctx.guild.name
    msg = await ctx.send(GetPhrase('weekly_registration_resetting'))
    if path.exists("serverfiles/tournaments/" + server_name + "-unfiltered.json"):
        os.remove("serverfiles/tournaments/" + server_name + "-unfiltered.json")
        await msg.edit(content=GetPhrase('weekly_registration_end'))
    else:
        await msg.edit(content=GetPhrase('weekly_registration_missing'))


async def CheckinWeekly(ctx, name):
    msg = await ctx.send(GetPhrase('weekly_checkin'))
    server_name = ctx.guild.name
    player_found = False
    if path.exists("serverfiles/tournaments/" + server_name + "-unfiltered.json"):
        with open("serverfiles/tournaments/" + server_name + "-unfiltered.json", encoding="utf8") as f:
            registered_participants = json.load(f)
        if not registered_participants['checkin_started']:
            await msg.edit(content=GetPhrase('weekly_checkin_notopen'))
            return
        for player in registered_participants['players']:
            if player['name'] == str(name):
                player_found = True
                if not player['checked_in']:
                    player['checked_in'] = True
                else:
                    await msg.edit(content=GetPhrase('weekly_checkin_already'))
                    return
        with open('serverfiles/tournaments/' + ctx.guild.name + '-unfiltered.json', 'w+', encoding='utf-8') as f:
            json.dump(registered_participants, f, ensure_ascii=False, indent=4)
    if not player_found:
        await msg.edit(content=GetPhrase('weekly_checkin_notreg'))
    else:
        await msg.edit(content=GetPhrase('weekly_checkin_success'))


async def ToggleWeeklyCheckin(ctx):
    msg = await ctx.send(GetPhrase('weekly_checkin_toggle'))

    server_name = ctx.guild.name
    if path.exists("serverfiles/tournaments/" + server_name + "-unfiltered.json"):
        with open("serverfiles/tournaments/" + server_name + "-unfiltered.json", encoding="utf8") as f:
            registered_participants = json.load(f)
        registered_participants['checkin_started'] = not registered_participants['checkin_started']
        with open('serverfiles/tournaments/' + ctx.guild.name + '-unfiltered.json', 'w+', encoding='utf-8') as f:
            json.dump(registered_participants, f, ensure_ascii=False, indent=4)
    else:
        await msg.edit(content=GetPhrase('weekly_registration_missing'))
        return

    if registered_participants['checkin_started']:
        await msg.edit(content=GetPhrase('weekly_checkin_open'))
    else:
        await msg.edit(content=GetPhrase('weekly_checkin_closed'))


async def Help(ctx, query):
    help_message = ""
    if path.exists("resources/commandshelp.json"):
        with open("resources/commandshelp.json", encoding="utf8") as f:
            commands = json.load(f)
    if query is None:
        help_message = GetPhrase('help_description') + "\n```\n"
        for command in commands['commands']:
            help_message = help_message + command['name'] + ", "
        help_message = help_message + "```"
        await ctx.send(help_message)
        return
    elif query in commands['categories']:
        help_message = "**Commands in category** **" + query + "**\n```\n"
        for command in commands['commands']:
            if command['category'] == query:
                help_message = help_message + "--- " + command['name'] + "\n"
                help_message = help_message + command['description'] + "\n"
                if command['aliases'] != "":
                    help_message = help_message + "Aliases: " + command['aliases'] + "\n"
                help_message = help_message + "\n"
    else:
        for command in commands['commands']:
            if command['name'] == query:
                help_message = "**Command found**: \n```\n"
                help_message = help_message + "Command:     " + "[" + command['name'] + "]" + "\n"
                if command['aliases'] != "":
                    help_message = help_message + "Aliases:     " + command['aliases'] + "\n"
                help_message = help_message + "Category:    " + command['category'] + "\n"
                help_message = help_message + "Example:     " + command['example'] + "\n"
                help_message = help_message + "Description: " + command['description'] + "\n"

    if help_message != "":
        help_message = help_message + "```"
        await ctx.send(help_message)
    else:
        await ctx.send(GetPhrase('help_not_found'))


def GetPhrase(phrase):
    data = None
    if path.exists("resources/phrases.json"):
        with open("resources/phrases.json", encoding="utf8") as f:
            data = json.load(f)
    return "**" + data[phrase] + "**"


#SETUP AND ADMIN:

async def AdminCheck(ctx, user):
    admin = database.CheckAdmin(ctx.guild.name)
    if admin is None:
        await ctx.send(GetPhrase('fail_admin_none'))
        return 0
    else:
        for role in user.roles:
            if admin["role"] == role.name:
                return 2
    await ctx.send(GetPhrase('fail_permissions'))
    return 1


def InitializeAdmin(role, server):
    database.AddAdmin(role, server)


def UpdateAdmin(role, server):
    database.UpdateAdmin(role, server)


def CreateRank(role, game, server, _min, _max, role_id):
    database.AddRank(role, game, server, _min, _max, role_id)


def RemoveRank(role, game, server):
    database.RemoveRank(role, game, server)


def SetDefaultRank(role, points, game, server):
    return database.SetDefaultRank(role, points, game, server)


def GetDefaultRank(game, server):
    return database.GetDefaultRank(game, server)


def GetRanks(game, server):
    response = database.GetRanks(game, server)
    field_names = ['Game', 'Role', 'Min', 'Max']
    table = []
    for rank in response:
        del rank["role_id"]
        table.append(FormatResponse(rank))
    return FormatTable(field_names, table)


def DecodeUTF(t):
    return str(t).encode('ascii', 'replace').decode()

