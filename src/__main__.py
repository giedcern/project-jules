# bot.py

import modules.botcommands as bot_commands


def main():
    bot_commands.start()


if __name__ == '__main__':
    main()
